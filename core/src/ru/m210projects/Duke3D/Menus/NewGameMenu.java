// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Menus;

import static ru.m210projects.Duke3D.Factory.DukeMenuHandler.*;
import static ru.m210projects.Duke3D.Globals.*;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuButton;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Pattern.MenuItems.MenuProc;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Duke3D.Factory.DukeMenuHandler;

public class NewGameMenu extends BuildMenu {

	public NewGameMenu(final Main app)
	{
		final DukeMenuHandler menu = (DukeMenuHandler) app.menu;

		addItem(new DukeTitle("SELECT AN EPISODE"), false);
		
		MenuProc newEpProc = new MenuProc() {
			@Override
			public void run( MenuHandler handler, MenuItem pItem ) {
				EpisodeButton but = (EpisodeButton) pItem;
				DifficultyMenu next = (DifficultyMenu) menu.mMenus[DIFFICULTY];
				next.setEpisode(but.game, but.specialOpt);
				menu.mOpen(next, but.nItem);
			}
		};

		int epnum = 0;
		int pos = 30;
		for(int i = 0; i < defGame.nEpisodes; i++)
		{
			if(defGame.episodes[i] != null) { //empty check
				EpisodeButton skill = new EpisodeButton(defGame, defGame.episodes[i].Title, app.getFont(2), 0, pos+=19, 320, newEpProc, i);
				addItem(skill, i == 0);
				epnum++;
			}
		}
		
		final DUserContent usercont = (DUserContent) menu.mMenus[USERCONTENT];
		MenuButton mUser = new MenuButton("< USER CONTENT >", app.getFont(2), 0, pos+=19, 320, 1, 0, null, -1, new MenuProc() {
			@Override
			public void run(MenuHandler handler, MenuItem pItem) {
				if(usercont.showmain) 
					usercont.setShowMain(false);
				handler.mOpen(usercont, -1);
			}
		}, -1);
		addItem(mUser, epnum == 0);
	}
}
