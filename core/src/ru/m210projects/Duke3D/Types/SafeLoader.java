// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Types;

import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Engine.MAXPSKYTILES;
import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Engine.MAXWALLS;
import static ru.m210projects.Build.Engine.MAXWALLSV7;
import static ru.m210projects.Duke3D.Animate.MAXANIMATES;
import static ru.m210projects.Duke3D.Gamedef.MAXSCRIPTSIZE;
import static ru.m210projects.Duke3D.Globals.MAXANIMWALLS;
import static ru.m210projects.Duke3D.Globals.MAXCYCLERS;
import static ru.m210projects.Duke3D.LoadSave.SAVEGDXDATA;
import static ru.m210projects.Duke3D.LoadSave.SAVEHEADER;
import static ru.m210projects.Duke3D.LoadSave.SAVELEVELINFO;
import static ru.m210projects.Duke3D.LoadSave.SAVESCREENSHOTSIZE;
import static ru.m210projects.Duke3D.ResourceHandler.levelGetEpisode;
import static ru.m210projects.Build.Strhandler.*;

import ru.m210projects.Build.FileHandle.Resource.ResourceData;
import ru.m210projects.Build.Types.SECTOR;
import ru.m210projects.Build.Types.SPRITE;
import ru.m210projects.Build.Types.WALL;

public class SafeLoader {

	public String boardfilename;
	public GameInfo addon;
	public String addonFileName;
	public byte warp_on;
	private String message;

	public int gAnimationCount = 0;
	public ANIMATION[] gAnimationData = new ANIMATION[MAXANIMATES];

	public short pskyoff[] = new short[MAXPSKYTILES], pskybits;
	public int parallaxyscale;
	public short connecthead, connectpoint2[] = new short[MAXPLAYERS];

	public int randomseed;
	public int visibility;
	public byte automapping;
	public byte[] show2dsector = new byte[(MAXSECTORS + 7) >> 3];
	public byte[] show2dwall = new byte[(MAXWALLSV7 + 7) >> 3];
	public byte[] show2dsprite = new byte[(MAXSPRITES + 7) >> 3];

	// MapInfo

	public short numsectors, numwalls;

	public short[] headspritesect, headspritestat;
	public short[] prevspritesect, prevspritestat;
	public short[] nextspritesect, nextspritestat;

	public SECTOR[] sector = new SECTOR[MAXSECTORS];
	public WALL[] wall = new WALL[MAXWALLS];
	public SPRITE[] sprite = new SPRITE[MAXSPRITES];

	// GameInfo

	public PlayerStruct ps[] = new PlayerStruct[MAXPLAYERS];
	public PlayerOrig po[] = new PlayerOrig[MAXPLAYERS];
	public Weaponhit[] hittype = new Weaponhit[MAXSPRITES];

	public int multimode, volume_number, level_number, player_skill;
	public int from_bonus, secretlevel;
	public boolean respawn_monsters, respawn_items, respawn_inventory, god, monsters_off;
	public int auto_run, crosshair, last_level, eog, coop, marker, ffire;
	public short numplayersprites, global_random, earthquaketime, camsprite;
	public short frags[][] = new short[MAXPLAYERS][MAXPLAYERS];

	public int[] msx = new int[2048], msy = new int[2048];
	public Animwalltype animwall[] = new Animwalltype[MAXANIMWALLS];
	public short cyclers[][] = new short[MAXCYCLERS][6], numcyclers;
	public short numanimwalls;
	public short spriteq[] = new short[1024];
	public short spriteqloc, spriteqamount;
	public short mirrorwall[] = new short[64], mirrorsector[] = new short[64], mirrorcnt;
	public short numclouds, clouds[] = new short[128], cloudx[] = new short[128], cloudy[] = new short[128];

	public int actorscrptr[] = new int[MAXTILES];
	public short actortype[] = new short[MAXTILES];
	public int script[] = new int[MAXSCRIPTSIZE];

	public SafeLoader() {
		for (int i = 0; i < MAXPLAYERS; i++) {
			ps[i] = new PlayerStruct();
			po[i] = new PlayerOrig();
		}

		for (int i = 0; i < MAXANIMWALLS; i++)
			animwall[i] = new Animwalltype();
		for (int i = 0; i < MAXSPRITES; i++)
			hittype[i] = new Weaponhit();

		headspritesect = new short[MAXSECTORS + 1];
		headspritestat = new short[MAXSTATUS + 1];
		prevspritesect = new short[MAXSPRITES];
		prevspritestat = new short[MAXSPRITES];
		nextspritesect = new short[MAXSPRITES];
		nextspritestat = new short[MAXSPRITES];

		for (int i = 0; i < MAXANIMATES; i++)
			gAnimationData[i] = new ANIMATION();
		for (int i = 0; i < MAXSPRITES; i++)
			sprite[i] = new SPRITE();
		for (int i = 0; i < MAXSECTORS; i++)
			sector[i] = new SECTOR();
		for (int i = 0; i < MAXWALLS; i++)
			wall[i] = new WALL();
	}

	public void LoadGDXBlock(ResourceData bb) {
		int pos = bb.position();
		// reserve SAVEGDXDATA bytes for extra data

		warp_on = bb.get();
		if (warp_on == 1) {
			byte[] buf = new byte[144];
			bb.get(buf);
			addonFileName = toLowerCase(new String(buf).trim());
		}

		bb.position(pos + SAVEGDXDATA);
	}

	public void MapLoad(ResourceData bb) {
		byte[] buf = new byte[144];
		bb.get(buf);

		boardfilename = null;
		String name = new String(buf).trim();
		if (!name.isEmpty())
			boardfilename = name;

		numwalls = bb.getShort();
		for (int w = 0; w < numwalls; w++) {
			wall[w] = new WALL();
			wall[w].buildWall(bb);
		}

		numsectors = bb.getShort();
		for (int s = 0; s < numsectors; s++) {
			sector[s] = new SECTOR();
			sector[s].buildSector(bb);
		}

		// Store all sprites (even holes) to preserve indeces
		for (int i = 0; i < MAXSPRITES; i++) {
			sprite[i] = new SPRITE();
			sprite[i].buildSprite(bb);
		}

		for (int i = 0; i <= MAXSECTORS; i++)
			headspritesect[i] = bb.getShort();
		for (int i = 0; i <= MAXSTATUS; i++)
			headspritestat[i] = bb.getShort();

		for (int i = 0; i < MAXSPRITES; i++) {
			prevspritesect[i] = bb.getShort();
			prevspritestat[i] = bb.getShort();
			nextspritesect[i] = bb.getShort();
			nextspritestat[i] = bb.getShort();
		}
	}

	public void AnimationLoad(ResourceData bb) {
		for (int i = 0; i < MAXANIMATES; i++) {
			short index = bb.getShort();
			byte type = bb.get();
			gAnimationData[i].id = index;
			gAnimationData[i].type = type;
			gAnimationData[i].ptr = null;
			gAnimationData[i].goal = bb.getInt();
			gAnimationData[i].vel = bb.getInt();
			gAnimationData[i].sect = bb.getShort();
		}
		gAnimationCount = bb.getInt();
	}

	public void Stuff2Load(ResourceData bb) {
		pskybits = bb.getShort();
		parallaxyscale = bb.getInt();
		for (int i = 0; i < MAXPSKYTILES; i++)
			pskyoff[i] = bb.getShort();

		earthquaketime = bb.getShort();
		from_bonus = bb.getShort();
		secretlevel = bb.getShort();
		respawn_monsters = bb.get() == 1;
		respawn_items = bb.get() == 1;
		respawn_inventory = bb.get() == 1;
		god = bb.get() == 1;
		auto_run = (bb.getInt() == 1) ? 1 : 0;
		crosshair = (bb.getInt() == 1) ? 1 : 0;
		monsters_off = bb.get() == 1;
		last_level = bb.getInt();
		eog = bb.getInt();
		coop = bb.getInt();
		marker = bb.getInt();
		ffire = bb.getInt();
		camsprite = bb.getShort();

		connecthead = bb.getShort();
		for (int i = 0; i < MAXPLAYERS; i++)
			connectpoint2[i] = bb.getShort();
		numplayersprites = bb.getShort();

		for (int i = 0; i < MAXPLAYERS; i++)
			for (int j = 0; j < MAXPLAYERS; j++)
				frags[i][j] = bb.getShort();

		randomseed = bb.getInt();
		global_random = bb.getShort();
	}

	public void StuffLoad(ResourceData bb) {
		numcyclers = bb.getShort();
		for (int i = 0; i < MAXCYCLERS; i++)
			for (int j = 0; j < 6; j++)
				cyclers[i][j] = bb.getShort();

		for (int i = 0; i < MAXPLAYERS; i++)
			ps[i].set(bb);
		for (int i = 0; i < MAXPLAYERS; i++)
			po[i].set(bb);

		numanimwalls = bb.getShort();
		for (int i = 0; i < MAXANIMWALLS; i++) {
			animwall[i].wallnum = bb.getShort();
			animwall[i].tag = bb.getInt();
		}
		for (int i = 0; i < 2048; i++)
			msx[i] = bb.getInt();
		for (int i = 0; i < 2048; i++)
			msy[i] = bb.getInt();

		spriteqloc = bb.getShort();
		spriteqamount = bb.getShort();
		for (int i = 0; i < 1024; i++)
			spriteq[i] = bb.getShort();

		mirrorcnt = bb.getShort();
		for (int i = 0; i < 64; i++)
			mirrorwall[i] = bb.getShort();
		for (int i = 0; i < 64; i++)
			mirrorsector[i] = bb.getShort();

		bb.get(show2dsector);
		numclouds = bb.getShort();
		for (int i = 0; i < 128; i++)
			clouds[i] = bb.getShort();
		for (int i = 0; i < 128; i++)
			cloudx[i] = bb.getShort();
		for (int i = 0; i < 128; i++)
			cloudy[i] = bb.getShort();
	}

	public void ConLoad(ResourceData bb) {
		for (int i = 0; i < MAXTILES; i++)
			actortype[i] = (short) (bb.get() & 0xFF);
		for (int i = 0; i < MAXSCRIPTSIZE; i++)
			script[i] = bb.getInt();
		for (int i = 0; i < MAXTILES; i++)
			actorscrptr[i] = bb.getInt();
		for (int i = 0; i < MAXSPRITES; i++)
			hittype[i].set(bb);
	}

	public String getMessage() {
		return message;
	}

	public boolean load(ResourceData bb) {
		addon = null;
		addonFileName = null;
		message = null;
		warp_on = 0;

		try {
			bb.position(SAVEHEADER - SAVELEVELINFO);

			multimode = bb.getInt();
			volume_number = bb.getInt();
			level_number = bb.getInt();
			player_skill = bb.getInt();

			bb.position(SAVEHEADER + SAVESCREENSHOTSIZE);

			LoadGDXBlock(bb);
			MapLoad(bb);
			StuffLoad(bb);
			ConLoad(bb);
			AnimationLoad(bb);
			Stuff2Load(bb);

			if (warp_on == 1) { // try to find addon
				addon = levelGetEpisode(addonFileName);
				if (addon == null) {
					message = "Can't find user episode file: " + addonFileName;
					warp_on = 2;

					volume_number = 0;
					level_number = 7;
				}
			}

			if (bb.position() == bb.capacity())
				return true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
