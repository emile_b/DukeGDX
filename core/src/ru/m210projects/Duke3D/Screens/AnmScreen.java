// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Screens;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Input.Keymap.ANYKEY;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Duke3D.Globals.TILE_ANIM;
import static ru.m210projects.Duke3D.Globals.ud;
import static ru.m210projects.Duke3D.SoundDefs.BIGBANG;
import static ru.m210projects.Duke3D.SoundDefs.BOSS4_DEADSPEECH;
import static ru.m210projects.Duke3D.SoundDefs.DUKE_GRUNT;
import static ru.m210projects.Duke3D.SoundDefs.DUKE_UNDERWATER;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND1;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND2;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND3;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND4;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND5;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND6;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL2SND7;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND2;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND3;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND5;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND6;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND7;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND8;
import static ru.m210projects.Duke3D.SoundDefs.ENDSEQVOL3SND9;
import static ru.m210projects.Duke3D.SoundDefs.FLY_BY;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_1;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_2;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_3;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_4;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_5;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_6;
import static ru.m210projects.Duke3D.SoundDefs.INTRO4_B;
import static ru.m210projects.Duke3D.SoundDefs.PIPEBOMB_EXPLODE;
import static ru.m210projects.Duke3D.SoundDefs.SHORT_CIRCUIT;
import static ru.m210projects.Duke3D.SoundDefs.SQUISHED;
import static ru.m210projects.Duke3D.SoundDefs.THUD;
import static ru.m210projects.Duke3D.SoundDefs.VOL4ENDSND1;
import static ru.m210projects.Duke3D.SoundDefs.VOL4ENDSND2;
import static ru.m210projects.Duke3D.SoundDefs.WIND_AMBIENCE;
import static ru.m210projects.Duke3D.SoundDefs.WIND_REPEAT;
import static ru.m210projects.Duke3D.Sounds.StopAllSounds;
import static ru.m210projects.Duke3D.Sounds.sndStopMusic;
import static ru.m210projects.Duke3D.Sounds.sound;

import com.badlogic.gdx.Gdx;

import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.Audio.Source;
import ru.m210projects.Build.Pattern.BuildFont.TextAlign;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Duke3D.Types.AnimFile;

public class AnmScreen extends SkippableAdapter {

	private Runnable callback;
	private int gCutsClock;
	private AnimFile anmfil;
	private int lastanimhack;
	private int frame;
	private long anmtime;
	private long LastMS;
	private String name;
	
	private int bonuscnt = 0;
	private Source scenevoice;
	private int scenevoices[] = {
		ENDSEQVOL3SND5, ENDSEQVOL3SND6, ENDSEQVOL3SND7, ENDSEQVOL3SND8, ENDSEQVOL3SND9
	};

	public AnmScreen(BuildGame game) {
		super(game);
	}
	
	@Override
	public void show() {
		if(game.pMenu.gShowMenu)
			game.pMenu.mClose();

		engine.sampletimer();
		LastMS = engine.getticks();
		gCutsClock = totalclock = 0;
		
		StopAllSounds();
		sndStopMusic();
	}
	
	@Override
	public void hide () {
		engine.setbrightness(ud.brightness>>2, palette, 2);
	}

	@Override
	public void skip() {
		anmClose();
		super.skip();
	}
	
	public AnmScreen setCallback(Runnable callback) {
		this.callback = callback;
		this.setSkipping(callback);
		return this;
	}
	
	public boolean init(String fn, int t)
	{
		if(anmfil != null) return false;
		
		byte[] animbuf = BuildGdx.cache.getBytes(fn, 0);
		if(animbuf == null) return false;

	    try {
	    	anmfil = new AnimFile(animbuf);

		    tilesizx[TILE_ANIM] = 200;
		    tilesizy[TILE_ANIM] = 320;
		    lastanimhack = t;
		    frame = 1;
		    
		    anmtime = 0;
			LastMS = -1;
			name = fn;

		    waloff[TILE_ANIM] = null;
		    
		    byte[] pal = anmfil.getPalette();
		    engine.changepalette(pal);
		    
		    int white = -1;
		    int k = 0;
	        for (int i = 0; i < 256; i+=3)
	        {
	            int j = (pal[3*i]&0xFF)+(pal[3*i+1]&0xFF)+(pal[3*i+2]&0xFF);
	            if (j > k) { k = j; white = i; }
	        }
		    
	        int palnum = MAXPALOOKUPS - RESERVEDPALS - 1;
		    byte[] remapbuf = new byte[768];
			for(int i = 0; i < 768; i++)
				remapbuf[i] = (byte) white;	
			engine.makepalookup(palnum, remapbuf,0, 1, 0, 1);
			
			for(int i = 0; i < 256; i++) {
				int tile = game.getFont(0).getTile(i);
				if(tile >= 0) 
					engine.invalidatetile(tile, palnum, -1);
			}
	        
		    return true;
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return false;
	    }
	}
	
	private boolean anmPlay()
	{
		if(anmfil != null) {
			if(LastMS == -1) 
				LastMS = engine.getticks();

			long ms = engine.getticks();
			long dt = ms - LastMS;
			anmtime += dt;
			float tick = 1000f / anmfil.getRate();
			if(anmtime >= tick) {
				if(frame < anmfil.numFrames()) {
					waloff[TILE_ANIM] = anmfil.draw(frame);
					engine.invalidatetile(TILE_ANIM, 0, -1);	// JBF 20031228

					if(lastanimhack == 8) endanimvol41(frame);
					else if(lastanimhack == 10) endanimvol42(frame);
					else if(lastanimhack == 11) endanimvol43(frame);
					else if(lastanimhack == 9) intro42animsounds(frame);
					else if(lastanimhack == 7) intro4animsounds(frame);
					else if(lastanimhack == 6) first4animsounds(frame);
					else if(lastanimhack == 5) logoanimsounds(frame);
					else if(lastanimhack < 4) endanimsounds(frame);
					
					frame++;
				} 

				anmtime -= tick;
			}
			
			LastMS = ms;
			if(tilesizx[TILE_ANIM] <= 0)
				return false;

			if(waloff[TILE_ANIM] != null) 
				engine.rotatesprite(0<<16,0<<16,65536,512,TILE_ANIM,0,0,2+4+8+16+64, 0,0,xdim-1,ydim-1);
			
			if(frame >= anmfil.numFrames())
				return false;

			return true;
		}

		return false;
	}

	@Override
	public void draw(float delta) {
		if(!anmPlay() && skipCallback != null) {
			if(!checkAnm()) {
				anmClose();
				if (callback != null) {
					Gdx.app.postRunnable(callback);
					callback = null;
				}
			}
		}
		
		if (game.pInput.ctrlKeyStatus(ANYKEY)) 
			gCutsClock = totalclock;
		
		int shade = 16 + mulscale(16, sintable[(20 * totalclock) & 2047], 16);
		if (totalclock - gCutsClock < 200 && escSkip) // 2 sec 
			game.getFont(0).drawText(160, 5, "Press ESC to skip", shade, MAXPALOOKUPS - RESERVEDPALS - 1, TextAlign.Center, 2, true);
	}
	
	private boolean checkAnm()
	{
		if(name.equals("vol41a.anm")) {
			anmClose();
			init("vol42a.anm", 7);
			return true;
		}
		
		if(name.equals("vol42a.anm")) {
			anmClose();
			init("vol43a.anm", 9);
			return true;
		}
		
		if(name.equals("vol4e1.anm")) {
			anmClose();
			init("vol4e2.anm", 10);
			return true;
		}
		
		if(name.equals("vol4e2.anm")) {
			anmClose();
			init("vol4e3.anm", 11);
			return true;
		}
		
		if(name.equals("radlogo.anm")) {
        	if(bonuscnt >= 0 && bonuscnt < scenevoices.length) {
        		if(scenevoice == null || !scenevoice.isActive()) {
        			scenevoice = sound(scenevoices[bonuscnt]);
        			bonuscnt++;
        		}	
        	}
			return true; //no autoskip
		}

		if(name.equals("duketeam.anm")) 
			return true; //no autoskip
		
		return false;
	}

	private void anmClose() {
		anmfil = null;
	}

	private void endanimsounds(int fr)
	{
	    switch(ud.volume_number)
	    {
	        case 0:break;
	        case 1:
	            switch(fr)
	            {
	                case 1:
	                    sound(WIND_AMBIENCE);
	                    break;
	                case 26:
	                    sound(ENDSEQVOL2SND1);
	                    break;
	                case 36:
	                    sound(ENDSEQVOL2SND2);
	                    break;
	                case 54:
	                    sound(THUD);
	                    break;
	                case 62:
	                    sound(ENDSEQVOL2SND3);
	                    break;
	                case 75:
	                    sound(ENDSEQVOL2SND4);
	                    break;
	                case 81:
	                    sound(ENDSEQVOL2SND5);
	                    break;
	                case 115:
	                    sound(ENDSEQVOL2SND6);
	                    break;
	                case 124:
	                    sound(ENDSEQVOL2SND7);
	                    break;
	            }
	            break;
	        case 2:
	            switch(fr)
	            {
	                case 1:
	                    sound(WIND_REPEAT);
	                    break;
	                case 98:
	                    sound(DUKE_GRUNT);
	                    break;
	                case 82+20:
	                    sound(THUD);
	                    sound(SQUISHED);
	                    break;
	                case 104+20:
	                    sound(ENDSEQVOL3SND3);
	                    break;
	                case 114+20:
	                    sound(ENDSEQVOL3SND2);
	                    break;
	                case 158:
	                    sound(PIPEBOMB_EXPLODE);
	                    break;
	            }
	            break;
	    }
	}

	private void logoanimsounds(int fr)
	{
	    switch(fr)
	    {
	        case 1:
	            sound(FLY_BY);
	            break;
	        case 19:
	            sound(PIPEBOMB_EXPLODE);
	            break;
	    }
	}

	private void intro4animsounds(int fr)
	{
	    switch(fr)
	    {
	        case 1:
	            sound(INTRO4_B);
	            break;
	        case 12:
	        case 34:
	            sound(SHORT_CIRCUIT);
	            break;
	        case 18:
	            sound(INTRO4_5);
	            break;
	    }
	}

	private void first4animsounds(int fr)
	{
	    switch(fr)
	    {
	        case 1:
	            sound(INTRO4_1);
	            break;
	        case 12:
	            sound(INTRO4_2);
	            break;
	        case 7:
	            sound(INTRO4_3);
	            break;
	        case 26:
	            sound(INTRO4_4);
	            break;
	    }
	}

	private void intro42animsounds(int fr)
	{
	    switch(fr)
	    {
	        case 10:
	            sound(INTRO4_6);
	            break;
	    }
	}

	private void endanimvol41(int fr)
	{
	    switch(fr)
	    {
	        case 3:
	            sound(DUKE_UNDERWATER);
	            break;
	        case 35:
	            sound(VOL4ENDSND1);
	            break;
	    }
	}

	private void endanimvol42(int fr)
	{
	    switch(fr)
	    {
	        case 11:
	            sound(DUKE_UNDERWATER);
	            break;
	        case 20:
	            sound(VOL4ENDSND1);
	            break;
	        case 39:
	            sound(VOL4ENDSND2);
	            break;
	        case 50:
	        	BuildGdx.audio.getSound().stopAllSounds();
	            break;
	    }
	}

	private void endanimvol43(int fr)
	{
	    switch(fr)
	    {
	        case 1:
	            sound(BOSS4_DEADSPEECH);
	            break;
	        case 40:
	            sound(VOL4ENDSND1);
	            sound(DUKE_UNDERWATER);
	            break;
	        case 50:
	            sound(BIGBANG);
	            break;
	    }
	}

}
