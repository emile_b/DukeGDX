// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Fonts;

import static ru.m210projects.Build.Engine.tilesizx;
import static ru.m210projects.Build.Engine.tilesizy;
import static ru.m210projects.Duke3D.Names.BIGALPHANUM;
import static ru.m210projects.Duke3D.Names.BIGAPPOS;
import static ru.m210projects.Duke3D.Names.BIGCOLIN;
import static ru.m210projects.Duke3D.Names.BIGCOMMA;
import static ru.m210projects.Duke3D.Names.BIGPERIOD;
import static ru.m210projects.Duke3D.Names.BIGQ;
import static ru.m210projects.Duke3D.Names.BIGSEMI;
import static ru.m210projects.Duke3D.Names.BIGX;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildFont;

public class MenuFont extends BuildFont {

	public MenuFont(Engine draw) {
		super(draw, tilesizy[BIGALPHANUM], 65536, 8 | 16);

		this.addChar(' ', nSpace, 5, nScale, 0, 0);
		for(int i = 0; i < 26; i++) {
			int nTile = i + BIGALPHANUM;

			addChar((char) ('A' + i), nTile, tilesizx[nTile], nScale, 0, 0);
			addChar((char) ('a' + i), nTile, tilesizx[nTile], nScale, 0, 0);
		}
		
		for(int i = 0; i < 10; i++) {
			int nTile = i + BIGALPHANUM - 10;
			addChar((char) ('0' + i), nTile, tilesizx[nTile], nScale, 0, 0);
		}
		addChar('-', BIGALPHANUM-11, tilesizx[BIGALPHANUM-11], nScale, 0, 0);
		addChar('.', BIGPERIOD, tilesizx[BIGPERIOD], nScale, 0, 0);
		addChar(',', BIGCOMMA, tilesizx[BIGCOMMA], nScale, 0, 0);
		addChar('!', BIGX, tilesizx[BIGX], nScale, 0, 0);
		addChar('\'', BIGAPPOS, tilesizx[BIGAPPOS], nScale, 0, 0);
		addChar('?', BIGQ, tilesizx[BIGQ], nScale, 0, 0);
		addChar(';', BIGSEMI, tilesizx[BIGSEMI], nScale, 0, 0);
		addChar(':', BIGCOLIN, tilesizx[BIGCOLIN], nScale, 0, 0);
	}
	
	public void update()
	{
		
		for(int i = 0; i < 26; i++) {
			int nTile = i + BIGALPHANUM;

			charInfo[(char) ('A' + i)].nWidth = tilesizx[nTile];
			charInfo[(char) ('a' + i)].nWidth = tilesizx[nTile];
		}
		
		for(int i = 0; i < 10; i++) {
			int nTile = i + BIGALPHANUM - 10;
			charInfo[(char) ('0' + i)].nWidth = tilesizx[nTile];
		}
	}

}
